import ceylon.collection { HashSet, HashMap, LinkedList, MutableMap }

shared void testPlanetAttackStat() {
	
	value owner = NormalPlayer("zerg");
	value world = buildSimpleWorld(owner);
	value aw = AiWorld(world);
	
	print("start");
	//value ws = WorldState(w);
	//print(ws.connectedEnemyPlanets(z));
//	for (t in thoughts) {
	value worldState = WorldState(world);
	balance(worldState, owner);
	print(worldState.moves);
	balance(worldState, owner);

	AgressivenessCounter counter =  AgressivenessCounter();
	value pr = LinkedList<AttackProposal>();
	value postBalanceState = balance(worldState.fork(), owner);
	value postAttackState = handleAttacks(worldState.fork(), owner, counter);




	//}
	//print(thoughts);
	//balance(WorldState(w), z);
	print("finish");

}

shared void loop(WorldState ws, Player p, AgressivenessCounter counter) {
	print("New turn");
	balance(ws, p);
	print("Balance sequqnce completed");
	value attackWs = handleAttacks(ws, p, counter);
	print("Attack sequqnce completed");
	print(attackWs.moves);
	value concentratedWs = concentrate(attackWs, p);
	print("Concentration sequqnce completed");
	print(concentratedWs.moves);
	concentratedWs.submitMoves();
	print("Moves submitted");
}

shared class AgressivenessCounter() {
	variable Integer counter = 0;
	shared void inc() {
		counter += 1;
	}

	shared void reset() {
		counter = 0;
	}

	shared Integer get() {	
 		return counter;
	}
}

WorldState handleAttacks(WorldState worldState, Player owner, AgressivenessCounter counter) {
	value possibleAttackTargets = AiWorld(worldState.wolrd).connectedEnemyPlanets(owner);
	value proposals = LinkedList<AttackProposal>();
	variable value currentWorldState = worldState;
	variable Boolean changed = false;
	for (planet in possibleAttackTargets) {
		value forkedState = currentWorldState.fork(); 
		value attackProposal = tryFullAttack(planet, forkedState, owner);
		if (exists attackProposal) {
			if (isInstaFulfill(attackProposal, owner)) {
				print("Got instant fulfill attack proposal");
				changed = true;
				print("before: " + currentWorldState.moves.string);
				currentWorldState = attackProposal.ws;
				print("after: " + attackProposal.ws.moves.string);
			} else {
				proposals.add(attackProposal);
			}
		}
	}
	if (changed) {
		return handleAttacks(currentWorldState, owner, counter);
	} else {
		variable Float bestRatio = 0.float;
		variable AttackProposal? bestProposal = null;
		for (pr in proposals) {
			value ratio = evaluate(pr, owner);
			if (ratio > bestRatio) {
				bestProposal = pr;
			}
		}
		if (exists value prop = bestProposal) {
			if (bestRatio + counter.get() > 5.float) {
				counter.reset();
				return prop.ws;
			} else {
				print("no suitable attack target found");
				counter.inc();
			}
		} else {
			print("no suitable attack target found");
			counter.inc();
		}
	}
	return currentWorldState;
}

WorldState concentrate(WorldState ws, Player p) {
	value nav = AiWorld(ws.wolrd);
	value myPlanets = nav.playerPlanets(p);
	value smallestPlanet = myPlanets.fold(null, (Planet? partial, Planet elem) {
		if (exists partial) {
			if (ws.wolrd.getConnectedPlanets(elem).size > 1 && elem.buildRatio < partial.buildRatio) {
				return elem;
			} else {
				return partial;
			}
		} else {
			return elem;
		}
	});
	if (exists smallestPlanet) {
		print("Selected planet ``smallestPlanet`` as concentrationTarget");
		value request = PullRequest(FleetSize(100500), smallestPlanet);
		value connectedPlanets = ws.wolrd.getConnectedPlanets(smallestPlanet).filter((Planet elem) => elem.owner == p);
		for (connectedPlanet in connectedPlanets) {
			servePullRequest(ws.getPlanetState(connectedPlanet), ws.getPlanetState(smallestPlanet), ws, request, assignTiers(ws.wolrd, [connectedPlanet]));
		}
	}
	return ws;
}

Boolean isInstaFulfill(AttackProposal ap, Player owner) {
	value isNeutral = ap.request.consumer.owner is NeutralPlayer;
	value addsEnemies = ap.attackStat.addsEnemies;
	value targetPlanetState = ap.ws.getPlanetState(ap.request.consumer);
	value request = ap.request;
	value availableFleet = ap.request.fsize;

	if (isNeutral) {
		if (exists attackingFleet = targetPlanetState.totalIncomingNonFriendly) {
			return attackingFleet.size < FleetSize(3);
		}
	}
	if (addsEnemies == 0 && targetPlanetState.planet.buildRatio >= 0.13 && targetPlanetState.expectedOwner != owner) {
		return true;
	}
	if (addsEnemies == 1 && targetPlanetState.planet.buildRatio >= 0.16 && targetPlanetState.expectedOwner != owner) {	
		return true;
	}
	return false;
}
 

WorldState handleProposals({AttackProposal*} aps, Player owner) {
	for (ap in aps) {
		if (ap.request.consumer.owner is NeutralPlayer) {
			
		}
	}
	
	for (ap in aps) {
		
	}
	return nothing;
}

Float evaluate(AttackProposal proposal, Player owner) {
	value ws = proposal.ws;
	value request = proposal.request;
	value planet = request.consumer;
	value stat = PlanetAttackStat(planet, owner, ws.wolrd);
	value nav = AiWorld(ws.wolrd);
	value planetState = proposal.ws.getPlanetState(planet);
	
	Integer currentlyConnectedEnemies = nav.connectedPlayers(owner).size;
	Integer totalPlayers = (ws.wolrd.players.size - 2);
	variable Integer newEnemies = stat.addsEnemies;
	variable Integer blobbiness = stat.blobbiness;
	variable Integer quality = 1;
	Integer expectedNumberOfConnectedEnemies = currentlyConnectedEnemies + newEnemies;

	variable Float? newEnemiesRatio = stat.addsEnemies.float;
	variable Float? blobbinessRatio = stat.blobbiness.float;
	variable Float? qualityRatio = 1.float;
	value regen = planet.buildRatio;

	if (regen >= 0.2) {
		quality += 2;
	}
	if (regen >= 0.3) {
		quality += 5;
	}
	
	if (exists v = newEnemiesRatio) {
		// TODO: the less non conneccted players enemies exist the less likely connection should be created
	}
	
	
	// blobbiness and players count can be ignored if only one enemy exists
	if (totalPlayers == 1) {
		newEnemiesRatio = null;
		blobbinessRatio = null;
	}
	
	
	// blobbiness is less important than new enemies
	if (newEnemies > 1) {
		if (exists value b = blobbinessRatio) {
			blobbinessRatio = b * 0.5;
		}
	}
	
	// disconnected planets are very valueable
	if (nav.connectedplanetsFor(planet).size == 1) {
		if (exists br = blobbinessRatio) {
			blobbinessRatio = br + 100.float;
		}
	}
	
	value canWin = (planetState.expectedOwner == owner) then 10.float else 0.float;
	
	return ((newEnemiesRatio else 0.float) + (blobbinessRatio else 0.float) + (qualityRatio else 0.float) + canWin).float;
}

shared World buildSimpleWorld(Player z) {
	value n = NeutralPlayer();
	value p = NormalPlayer("Protoss");
	
	value w = World();
	w.addPlayers(p, n, z);
	value p1 = Planet("1", FleetSize(100), 0.1, z, FleetSize(95));
	value p2 = Planet("2", FleetSize(1000), 0.3, n);
	value p3 = Planet("3", FleetSize(200), 0.2, z, FleetSize(95));
	value p4 = Planet("4", FleetSize(200), 0.2, n);
	value p5 = Planet("5", FleetSize(1000), 0.3, p);
	value p6 = Planet("6", FleetSize(100), 0.1, p);
	w.addPlanets(p1, p2, p3, p4, p5, p6);
	
	w.addPath(Path(p1, p2));
	w.addPath(Path(p1, p4));
	w.addPath(Path(p2, p3));
	w.addPath(Path(p2, p5));
	w.addPath(Path(p4, p6));
	w.addPath(Path(p4, p5));
	w.addPath(Path(p5, p6));
	
	return w;
}

shared World build9x9World(Player z) {
	value p = NormalPlayer("Protoss");
	value n = NeutralPlayer();
	/*
	 value p1 = Planet("1", FleetSize(100), 0.1, z, FleetSize(100));
	 value p2 = Planet("2", FleetSize(100), 0.1, z, FleetSize(100));
	 value p3 = Planet("3", FleetSize(100), 0.1, n);
	 value p4 = Planet("4", FleetSize(100), 0.1, n);
	 value p5 = Planet("5", FleetSize(100), 0.1, n);
	 value p6 = Planet("6", FleetSize(100), 0.1, n);
	 value p7 = Planet("7", FleetSize(100), 0.1, n);
	 value p8 = Planet("8", FleetSize(100), 0.1, n);
	 value p9 = Planet("9", FleetSize(100), 0.1, p, FleetSize(100));
	 */
	
	value p1 = Planet("1", FleetSize(100), 0.1, z, FleetSize(100));
	value p2 = Planet("2", FleetSize(100), 0.1, z, FleetSize(100));
	value p3 = Planet("3", FleetSize(200), 0.1, z, FleetSize(100));
	value p4 = Planet("4", FleetSize(1000), 0.3, z, FleetSize(850));
	value p5 = Planet("5", FleetSize(100), 0.1, n);
	value p6 = Planet("6", FleetSize(100), 0.1, n);
	value p7 = Planet("7", FleetSize(100), 0.1, n);
	value p8 = Planet("8", FleetSize(100), 0.1, n);
	value p9 = Planet("9", FleetSize(100), 0.1, p, FleetSize(100));
	
	value w = World();
	w.addPlayers(z, p, n);
	w.addPlanets(p1, p2, p3, p4, p5, p6, p7, p8, p9);
	
	w.addPath(Path(p1, p4));
	w.addPath(Path(p1, p2));
	//w.addPath(Path(p1, p5));
	
	w.addPath(Path(p2, p5));
	w.addPath(Path(p2, p3));
	
	w.addPath(Path(p3, p6));
	
	w.addPath(Path(p4, p7));
	w.addPath(Path(p4, p5));
	
	w.addPath(Path(p5, p6));
	w.addPath(Path(p5, p8));
	//w.addPath(Path(p5, p9));
	
	w.addPath(Path(p6, p9));
	
	w.addPath(Path(p7, p8));
	w.addPath(Path(p8, p9));
	return w;
}

shared void ai(World world, Player p) {
	value ws = WorldState(world);
	balance(ws, p);
}

class AttackProposal(shared WorldState ws, shared PlanetAttackStat attackStat, shared PullRequest request) {
}

AttackProposal? tryFullAttack(Planet target, WorldState worldState, Player me) {
	print("Starting attack on ``target``");
	value myConnnectedPlanets = worldState.wolrd.getConnectedPlanets(target).filter((Planet elem) => elem.owner == me);
	value tieredPlanets = assignTiers(worldState.wolrd, [target]);
	value targetPlanetState = worldState.getPlanetState(target);
	value targetIsNeutral = targetPlanetState.planet.owner is NeutralPlayer;
	FleetSize attackFleetSize;
	
	if (targetIsNeutral && targetPlanetState.expectedOwner == me) {
		return null;
	} else if (targetIsNeutral) {
		attackFleetSize = FleetSize(1);
	} else {
		attackFleetSize = targetPlanetState.expectingShipsBeforeRegen // TODO: should consider help from connected planets
				.add(targetPlanetState.expectingShipsBeforeRegen)
				.add(FleetSize(1));
	}
	value pullRequest = PullRequest(attackFleetSize, target);
	for (planet in myConnnectedPlanets) {
		value planetState = worldState.getPlanetState(planet);
		print("Starting attack on ``target`` with fleet size of ``attackFleetSize`` from ``planetState.planet.name``");
		servePullRequest(planetState, targetPlanetState, worldState, pullRequest, tieredPlanets);
		print(planetState.outgoingMoves);
		print(worldState.moves);
	}
	print("Finished attack \n");
	return (pullRequest.fsize.compare(FleetSize(0)) == larger) then AttackProposal(worldState, PlanetAttackStat(target, me, worldState.wolrd), pullRequest) else null;
} 

void balance(WorldState ws, Player me) {
	print("balancing");
	value nav = AiWorld(ws.wolrd);
	value allMyPlanets = nav.playerPlanets(me);
	print(allMyPlanets);
	for (planet in allMyPlanets) {
		value planetState = ws.getPlanetState(planet);
		print(planetState);
		print(planetState.fleetSizeRequiredToFillOnNextTurn);
		if (exists value requiredFleetSize = planetState.fleetSizeRequiredToFillOnNextTurn) {
			value tieredPlanets = assignTiers(ws.wolrd, [planet]);
			print("Starting repopulation of ``planet`` which requires fleet size of ``requiredFleetSize``");
			value connectedPlanets = AiPlanet(planet, ws.wolrd).ownerPlanets;
			for (producerPlanet in connectedPlanets) {
				if (exists size = planetState.fleetSizeRequiredToFillOnNextTurn) {
					servePullRequest(ws.getPlanetState(producerPlanet.rawPlanet), planetState, ws, PullRequest(size, producerPlanet.rawPlanet), tieredPlanets);
				}
			}
			print("Finished repopulation \n\n\n");
		}
	}
	print("finished balancing");
}

shared class WorldState(World world) {
	variable value planets = HashMap<Planet, PlanetState>();
	
	shared PlanetState getPlanetState(Planet p) {
		value planetState = planets.get(p);
		if (exists planetState) {
			return planetState;
		} else {
			value retval = PlanetState(p);
			planets.put(p, retval);
			return retval;
		}
	}
	
	shared {PlanetState*} playerPlanets(Player player) {
		return world.planets.filter((Planet elem) => elem.owner == player).map(getPlanetState);
	}
	shared {PlanetState*} frontiers(Player owner) {
		return playerPlanets(owner).filter((PlanetState elem) => AiPlanet(elem.planet, world).isFrontier);
	}
	shared {PlanetState*} firstRow(Player owner) {
		return playerPlanets(owner)
				.filter((PlanetState elem) {
					return !AiPlanet(elem.planet, world).isFrontier && world.getConnectedPlanets(elem.planet).map(getPlanetState).containsAny(frontiers(owner));
				});
	}
	shared {PlanetState*} core(Player owner) {
		return  playerPlanets(owner).filter((PlanetState elem) {
			return !frontiers(owner).contains(getPlanetState(elem.planet)) && !firstRow(owner).contains(elem.planet);
		});
	}
	shared {PlanetState*} connectedplanetsFor(Planet planet) {
		return world.getConnectedPlanets(planet).map(getPlanetState);
	}
	shared {PlanetState*} connectedEnemyPlanets(Player owner) {
		return frontiers(owner).fold(HashSet<PlanetState>(), (HashSet<PlanetState> partial, PlanetState elem) {
			partial.addAll(world.getConnectedPlanets(elem.planet).map(getPlanetState).filter((PlanetState elem) => elem.planet.owner != owner)); 
			return partial;
		}).sequence;
	}
	
	shared {Player*} connectedPlayers(Player player) {
		return connectedEnemyPlanets(player).fold(HashSet<Player>(), (HashSet<Player> partial, PlanetState elem) {
			partial.add(elem.planet.owner);
			return partial;
		}).sequence;	
	}
	
	shared Integer numberOfConnectedEnemies(Player player) {
		return connectedPlayers(player).size;
	}
	
	
	shared World wolrd = world;
	
	shared {Move*} moves => planets.values.fold([], ({Move*} acc, PlanetState state) => acc.chain(state.outgoingMoves));
	
	shared void submitMoves() {
		value movesMap = HashMap<String, Move>();
		for (move in moves) {
			value key = move.from.name + "|" + move.to.name;
			if (exists savedMove = movesMap.get(key)) {
				value compoundMove = Move(move.from, move.to, move.number.add(savedMove.number), move.player);
				movesMap.put(key, compoundMove);
			} else {
				movesMap.put(key, move);
			}
		}
		for (move in movesMap.values) {
			world.submitMove(move);
		}
		print("submitting moves " + movesMap.values.string);
	}

	shared WorldState fork() {
		value forked = WorldState(world);
		for (k -> v in planets) {
			forked.planets.put(k, v.fork());
		}
		return forked;
	}
}

Map<Planet, Integer> assignTiers(World w, {Planet+} rootPlanets) {
	value planetsMap = HashMap<Planet, Integer> ();
	for(root in rootPlanets) {
		planetsMap.put(root, 0);
	}
	void assignTiersInner({Planet+} rootPlanets, Integer tier) {
		value allConnectedPlanets = rootPlanets.fold([], ({Planet*} list, Planet elem) => list.chain(w.getConnectedPlanets(elem)));
		value nextTierPlanets = LinkedList<Planet>();
		for (planet in allConnectedPlanets) {
			if (!planetsMap.keys.contains(planet)) {
				planetsMap.put(planet, tier);
				nextTierPlanets.add(planet);
			}
		} 
		if (nonempty searchplanets = nextTierPlanets.sequence) {
			assignTiersInner(searchplanets, tier + 1);
		}
	}
	assignTiersInner(rootPlanets, 1);
	return planetsMap;
}

void servePullRequest(PlanetState planet, PlanetState target, WorldState w, PullRequest r, Map<Planet, Integer> tiers) {
	print("Pull start: ``planet.planet.name`` got request from ``target.planet.name`` for ``r.size`` ships");
	
	value availableFreeShips = planet.freeShips;
	if (exists value as = availableFreeShips) {
		if (as >= r.size) { // has more than needed free ships
			fulfillRequest(r, planet, target, r.size, regen);
		} else {
			// does not have required number, use regen only
			fulfillRequest(r, planet, target, as, regen);
			askNetworkForMoreShips(planet, target, w, r, tiers);
		}
	} else {
		askNetworkForMoreShips(planet, target, w, r, tiers);
	}
	
	value friendlyTarget = target.planet.owner == planet.planet.owner;
	value targetHasBetterRegen = target.planet.buildRatio.float > planet.planet.buildRatio.float;
	
	
	// move units to the planets with beter regen
	if (friendlyTarget && targetHasBetterRegen) {
		if (exists fleetSizeTargetCanAccept = target.fleetSizeRequiredToFillOnNextTurn) {
			value minSize = min([r.size, fleetSizeTargetCanAccept, planet.maxDetachableSize]);
			if (!minSize.zero) {
				fulfillRequest(r, planet, target, minSize, Extra("better regen on target"));
			}
		}
	}
	print("Pull stop: ``planet.planet.name`` could provide ``r.fsize`` ships, ``r.size`` still required");
}

void askNetworkForMoreShips(PlanetState planet, PlanetState target, WorldState w, PullRequest r, Map<Planet, Integer> tiers) {
	print("``planet.planet.name`` needs ``r.size`` more ships from network");
	value planetList = AiPlanet(planet.planet, w.wolrd).ownerPlanets
			.sort((AiPlanet x, AiPlanet y) => x.rawPlanet.buildRatio.compare(y.rawPlanet.buildRatio.float));
	for (nextPlanet in planetList) {
		if ((tiers.get(nextPlanet.rawPlanet) else nothing) > (tiers.get(planet.planet) else nothing)) {
			print("``planet.planet.name`` asking ``nextPlanet.rawPlanet.name`` for ``r.size``");
			value newRequest = PullRequest(r.size, planet.planet);
			servePullRequest(w.getPlanetState(nextPlanet.rawPlanet), planet, w, newRequest, tiers);
			if (!newRequest.fsize.zero) {
				fulfillRequest(r, planet, target, newRequest.fsize, network);
			}
			print("``nextPlanet.rawPlanet.name`` could provide ``newRequest.fsize`` ships, ``r.size`` still required");
		}
	}
}

abstract class FulfillReason() of regen | network | Extra {
	
}

object regen extends FulfillReason() {}
object network extends FulfillReason() {}
class Extra(shared String reason) extends FulfillReason() {}

void fulfillRequest(PullRequest r, PlanetState from, PlanetState to, FleetSize size, FulfillReason reason) {
	value move = Move(from.planet, to.planet, size, from.planet.owner);
	from.addOutgoingMove(move);
	to.addIncomingMove(move);
	switch (reason)
	case(regen) {
		r.fulfillFromSurplus(size, from.planet);
		print("moving regen ``size`` from ``from.planet.name`` to ``to.planet.name``");	
	}
	case(network) {
		r.fulfillFromNetwork(size, from.planet);
		print("moving net ``size`` from ``from.planet.name`` to ``to.planet.name``");
		
	}
	case(is Extra) {
		r.fulfillFromSurplus(size, from.planet);
		print("moving ``reason.reason`` ``size`` from ``from.planet.name`` to ``to.planet.name``");
	}
}

shared class PlanetState(shared Planet planet) {
	variable LinkedList<Move> inc = LinkedList<Move>();
	variable LinkedList<Move> outg = LinkedList<Move>();
	shared {Move*} outgoingMoves => outg.sequence;
	shared {Move*} incomingMoves => inc.sequence;
	
	shared Player expectedOwner {
		if (exists value fleet = totalIncomingNonFriendly) {
			if (fleet.size > expectingShipsBeforeFight) {
				return fleet.owner;
			} else {
				return planet.owner;
			}
		} else {
			return planet.owner;
		}
	}
	
	shared void addOutgoingMove(Move move) {
		assert(move.from == this.planet);
		assert(inc.find((Move elem) => elem.from == move.to) is Null);
		outg.add(move);
	}
	shared void addIncomingMove(Move move) {
		assert(move.to == this.planet);
		assert(outg.find((Move elem) => elem.to == move.from) is Null);
		inc.add(move);
	}
	
	shared FleetSize allAvailableShips {
		return planet.getOwnerFleet().size.substract(totalLeaving) else nothing;
	}
	
	shared FleetSize totalIncomingFriendly {
		return incomingMoves
				.filter((Move move) => move.player == planet.owner)
				.fold(FleetSize(0), (FleetSize acc, Move elem) => acc.add(elem.number));
	}
	
	shared Fleet? totalIncomingNonFriendly {
		value enemyMoves = incomingMoves .filter((Move move) => move.player != planet.owner);
		value movesMap = enemyMoves.fold(HashMap<Player, FleetSize>(), (HashMap<Player, FleetSize> acc, Move elem) {
			acc.put(elem.player, (acc.get(elem.player) else FleetSize(0)).add(elem.number));
			return acc;
		});
		variable value largestFleetSize = FleetSize(0);
		variable Player? largestFleetOwner = null;
		for (k -> v in movesMap) {
			if (v > largestFleetSize) {
				largestFleetOwner = k;
				largestFleetSize = v;
			}
		}
		if (exists value lo =  largestFleetOwner) {
			return Fleet(lo, largestFleetSize);
		} else {
			return null;
		}
	}
	
	shared FleetSize totalLeaving {
		return outgoingMoves.fold(FleetSize(0), (FleetSize acc, Move elem) => acc.add(elem.number));
	}
	
	shared FleetSize expectingShipsBeforeRegen {
		return allAvailableShips.add(totalIncomingFriendly); // should count enemy ships as well
	}
	
	shared FleetSize expectingShipsBeforeFight {
		return expectingShipsBeforeRegen.mul(planet.buildRatio) else nothing;
	}
	
	shared Boolean belongsTo(Player p) => planet.owner == p;
	
	shared FleetSize? fleetSizeRequiredToFillOnNextTurn {
		return bestFleetSize.substract(expectingShipsBeforeRegen.add(FleetSize(1)));
	}
	
	shared FleetSize bestFleetSize {
		return planet.capacity.mul(1.float / (planet.buildRatio.float + 1.float)) else nothing;
	}
	
	shared FleetSize maxBuildSize => allAvailableShips.mul(planet.buildRatio.float) else nothing;
	shared FleetSize currentBuildSize {
		value max = maxBuildSize;
		value current = allAvailableShips;
		value capacity = planet.capacity;
		if (current.add(max) <= capacity) {
			return max;
		} else if (current.add(max) > capacity) {
			return FleetSize(0);
		} else {
			return capacity.substract(current) else nothing;
		}
	}
	
	shared FleetSize? freeShips {
		value bestSize = bestFleetSize;
		value ownerFleetSize = allAvailableShips;
		if (bestSize >= ownerFleetSize) {
			return null;
		} else {
			return ownerFleetSize.substract(bestSize) else nothing;
		}
	}
	
	shared FleetSize maxDetachableSize {
		return allAvailableShips.substract(FleetSize(1)) else FleetSize(0);
	}
	shared actual String string => "State for planet ``planet.name``";
	
	shared PlanetState fork() {
		value forked = PlanetState(planet);
		forked.inc = LinkedList(incomingMoves);
		forked.outg = LinkedList(outgoingMoves);
		return forked;
	}
}

void fullStrenghtRequest() {
}

shared class PlanetAttackStat(shared Planet target, Player owner, World world)  {
	value aw = AiWorld(world);
	value ap = AiPlanet(target, world);

	shared Integer addsEnemies {
		value currentEnemies = HashSet(aw.connectedPlayers(owner));
		value newEnemies = HashSet(ap.connectedPlayers(owner));
		value addedEnemies = newEnemies.complement(currentEnemies);
		return addedEnemies.size;
	}
	
	shared Integer blobbiness {
		return world.getConnectedPlanets(target).filter((Planet elem) => elem.owner == owner).size;
	}
	
	shared FleetSize ownerStrength {
		return calculateFleetSize(world, target.owner).add(calculateMaxRegen(world, target.owner));
	}
	
	shared actual String string => "Attack { target = ``target.name`` blob = ``blobbiness``, enemies = `` addsEnemies``, ownerStrenght = ``ownerStrength``";
}