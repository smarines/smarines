import ceylon.collection { LinkedList, HashMap, HashSet }
import ceylon.io { newServerSocket, SocketAddress, ServerSocket, Socket }
import ceylon.io.charset { utf8 }
import ceylon.file { parsePath, File, Nil, Writer }

shared abstract class Player(name) of NormalPlayer | NeutralPlayer {
	shared String name;
	actual shared String string = name;
}

class NormalPlayer(String name) extends Player(name) {

	shared default void accept(World world) {
		for (planet in world.planets) {
			if (planet.owner == this) {
				value connectedPlanets = world.getConnectedPlanets(planet);
				value enemyPlanet = connectedPlanets.find((Planet elem) => elem.owner != this);
				if (exists enemyPlanet) {
					value fleetToSend = planet.getOwnerFleet().size.div(2) else FleetSize(0);
					world.submitMove(Move(planet, enemyPlanet, fleetToSend, this));
					print("``this`` sending ``fleetToSend`` from ``planet`` to ``enemyPlanet``");
				}
			}
		}
	}
}

class AiPlayer(String name) extends NormalPlayer(name) {
	value ac = AgressivenessCounter();
	shared actual void accept(World world) {
		loop(WorldState(world), this, ac);
	}
} 

class HumanPlayer(String name, ServerSocket serverSocket) extends NormalPlayer(name) {
	variable Socket? socket = null;
	shared actual void accept(World world) {
		print("waiting connection");
		value socket = serverSocket.accept();
		this.socket = socket;
		print("got connection");
		value reader = Reader(socket);
		value xml = reader.nextToken();
		print("got xml: ``xml``");
		value moveStubs = parseRequest(xml.trimmed);
		for (moveStub in moveStubs) {
			try {
				value from = world.getPlanetByName(moveStub.from) else nothing;
				value to = world.getPlanetByName(moveStub.to) else nothing;
				value size = FleetSize(parseInteger(moveStub.num) else -1);
				value by = world.players.find((Player p) => p.name == moveStub.playerName) else nothing;
				world.submitMove(Move(from, to, size, by));
			} catch (Exception e) {
				print(e);
			}
		}
	}
	shared void respond(World world) {
		value socketCopy = socket;
		assert (exists socketCopy);
		value xml = worldToXml(world);
		print("sending worldstate: ");
		print(xml);
		socketCopy.writeFully(utf8.encode(xml));
		socketCopy.close();
	}
}

class NeutralPlayer() extends Player("neutral") { 
}

shared class FleetSize(Integer number) satisfies Comparable<FleetSize> {
	assert(number >= 0);
	
	Integer size = number;
	
	shared FleetSize? div(Integer times) {
		if (times.negative || times.zero) {
			return null;
		} else {
			return FleetSize(size / times);
		}
	}
	
	shared FleetSize? mul(Float times) {
		if(times.negative) {
			return null;
		} else {
			return FleetSize((size.float * times).integer);
		}
	}
	shared FleetSize add(FleetSize other) {
		return FleetSize(this.size + other.size);
	}
	shared FleetSize? substract(FleetSize other) {
		value val = this.size - other.size;
		if (!val.negative) {
			return FleetSize(val);
		} else {
			return null;
		}
	}
	
	actual shared String string = number.string;

	shared Boolean zero = number.zero;

	shared actual Comparison compare(FleetSize other) => this.size.compare(other.size);
	
}

shared class Planet(
	shared String name, 
	shared FleetSize capacity, 
	shared Float buildRatio, 
	shared variable Player owner, 
	shared FleetSize initialFleetSize = FleetSize(0)) {
	
	variable value combinedFleets = HashMap<Player, Fleet>();

	shared void addFleet(Fleet fleet) {
		value combinedFleet = (combinedFleets.get(fleet.owner) else Fleet(fleet.owner)).join(fleet);
		combinedFleets.put(fleet.owner, combinedFleet);
	}
	
	shared Fleet? riseFleet(Player owner, FleetSize newFleetSize) {
		value combinedFleet = (combinedFleets.get(owner) else Fleet(owner));
		value splittedFleet = combinedFleet.split(newFleetSize);
		if (exists splittedFleet) {
			combinedFleets.put(owner, splittedFleet.base);
			return splittedFleet.created;
		} else {
			return null;
		}
	}
	addFleet(Fleet(owner, initialFleetSize));

	shared void regen() {
		handleFleetBuilding();
	}
	
	shared void tick() {
		value largestFleets = getLargestTwoFleets();
		value largestFleet = largestFleets[0] else Fleet(NeutralPlayer());
		value secondLargestFleet = largestFleets[1] else Fleet(NeutralPlayer());
		value ownerFleet = getOwnerFleet();
		
		value ownerFleetSize = ownerFleet.size;
		value largestFleetSize = largestFleet.size;
		value secondLargestFleetSize = secondLargestFleet.size;

		if (ownerFleetSize >= largestFleetSize) {
			reset(Fleet(owner, ownerFleetSize.substract(largestFleetSize) else FleetSize(0)));
		} else {
			if (largestFleetSize == secondLargestFleetSize) {
				reset(Fleet(owner));
			} else {
				reset(Fleet(largestFleet.owner, largestFleetSize.substract(secondLargestFleetSize) else FleetSize(0)));
			}
		}
	}
	
	void reset(Fleet ownerFleet) {
		combinedFleets.clear();
		owner = ownerFleet.owner;
		addFleet(ownerFleet);
	}
	
	[Fleet?, Fleet?] getLargestTwoFleets() {
		return combinedFleets.values.fold([null, null], ([Fleet?, Fleet?] max, Fleet elem) {
			if (elem.owner == owner) { return max; }
			value fleetSize = elem.size;
			value veryMax = max[0];
			value preMax = max[1];
			if (exists veryMax) {
				if (exists preMax) {
					if (fleetSize >= veryMax.size) { return [elem, veryMax]; }
					else if (fleetSize < preMax.size) { return max; }
					else { return [veryMax, elem]; }
				} else {
					if (fleetSize >= veryMax.size) {
						return [elem, veryMax];
					} else {
						return [veryMax, elem];
					}
				}
			} else {
				return [elem, null];
			}
		});
	}
	
	shared Fleet getOwnerFleet() {
		return combinedFleets.get(owner) else Fleet(owner);
	}
	
	Fleet buildFleet() {
		return Fleet(owner, getOwnerFleet().size.mul(buildRatio.float) else FleetSize(0));
	}
	
	
	void handleFleetBuilding() {
		if (owner is NeutralPlayer) { return; }
		value builtFleet = buildFleet();
		value ownerFleet = getOwnerFleet();
		value builtFleetSize = builtFleet.size;
		value currentOwnerFleetSize = ownerFleet.size;
		value maxFleetSize = capacity;
		if (currentOwnerFleetSize >= maxFleetSize) {
			print("``builtFleet.size`` lost shps lost on ``this``!!");
		} else if (currentOwnerFleetSize.add(builtFleetSize) <= maxFleetSize) {
			addFleet(builtFleet);
		} else {
			value size = maxFleetSize.substract(currentOwnerFleetSize) else FleetSize(0);
			print("``builtFleet.size`` lost shps lost on ``this``!!");
			addFleet(Fleet(owner, size));
		}
	}
	
	actual shared String string {
		return "Planet: { name: ``name``, size: ``capacity`` owner: ``owner``, fleets: ``combinedFleets.values`` }";
	}
}

shared class Path(planetA, planetB) {
	assert(planetA != planetB);
	shared Planet planetA;
	shared Planet planetB;
}

shared class FleetSplitResult(base, created) {
	shared Fleet base;
	shared Fleet created;
}

shared class Fleet(owner, size = FleetSize(0)) {
	shared FleetSize size;
	shared Player owner;
	
	shared Fleet join(Fleet* fleets) {
		assert (! fleets.find((Fleet elem) => elem.owner != this.owner) exists);
		value newSize = fleets.fold(this.size, (FleetSize partial, Fleet elem) => partial.add(elem.size));
		return Fleet(this.owner, newSize);
	}
	
	shared FleetSplitResult? split(FleetSize newFleetSize) {
		value rest = size.substract(newFleetSize);
		if(exists rest) {
			return FleetSplitResult(Fleet(this.owner, rest), Fleet(this.owner, newFleetSize));
		} else {
			return null;
		}
	}
	shared actual String string = "Fleet { owner: ``owner``, size: ``size`` }";
}

shared class Move(from, to, number, player) {
	shared Planet from;
	shared Planet to;
	shared FleetSize number;
	shared Player player;
	
	shared actual String string => "Move { from = ``from.name``, to = ``to.name``, number = ``number`` }";
}

shared class MoveStub(shared String to, shared String from, shared String num, shared String playerName) {
	actual shared String string => "MoveStub: { to: ``to``, from: ``from``, num: ``num``, by: ``playerName`` }";
}


shared class World() {
	value path = parsePath("d:/asd.txt");
	variable Writer? writer = null;
	if (is Nil file =  path.resource) {
		file.createFile();
	}
	if (is File file = path.resource) {
		writer = file.Overwriter();
	}
	void log(String str) {
		//print(str);
		value wr = writer;
		if(exists wr) {
			wr.writeLine(str);
		}
	}
	value planetsList = LinkedList<Planet>();
	value playersList = LinkedList<Player>();
	value moves = LinkedList<Move>();
	value uncommitedMoves = LinkedList<FleetMove>();
	value connectedPlanetsMap = HashMap<Planet, LinkedList<Planet>>();
	
	void printStats() {
		log("===");
		log("end turn stats:");
		value map = HashMap<Player, FleetSize>();
		for (planet in planets) {
			value fleet = planet.getOwnerFleet();
			value curVal = map.get(fleet.owner) else FleetSize(0);
			map.put(fleet.owner, curVal.add(fleet.size));
		}
		log(map.string);
	}
	
	shared void go() {
		for (time in 1..2) {
			for (player in players) {
				switch(player)
					case (is NeutralPlayer) {}
					case (is NormalPlayer) {
						player.accept(this);
					}
			}
			log("\n\n\n reading moves");
			for (move in moves) {
				assert(move.to in getConnectedPlanets(move.from));
				value fleetMove = FleetMove(move.from, move.to, move.number, move.player);
				fleetMove.prepare();
				uncommitedMoves.add(fleetMove);
			}
			log("moves");
			for (move in uncommitedMoves) {
				try {
					log(move.string);
					move.commit();
				} catch (Exception e) {
					log(e.string);
				}
			}
			uncommitedMoves.clear();
			log("regen");
			for (planet in planets) {
				log("before regen: " + planet.string);
				planet.regen();
				log("after  regen: " + planet.string);
			}
			
			log("fight");
			for (planet in planets) {
				log("before tick: " + planet.string);
				planet.tick();
				log("after tick:  " + planet.string);
			}
			for (player in players) {
				if (is HumanPlayer player) {
					player.respond(this);
				}
			}
			moves.clear();
			printStats();
			log("\n\n\n\n\n\n\n");
			printStats();
		}
		log("finished");
		log(worldToXml(this));
	}
	
	class FleetMove(Planet from, Planet to, FleetSize fleetSize, Player owner) {
		variable Boolean prepared = false;
		variable Fleet? detachedFleet = null;
		
		shared void prepare() {
			if (from == to) {
				return;
			}
			value fleet = from.riseFleet(owner, fleetSize);
			if (exists fleet) {
				prepared = true;
				detachedFleet = fleet;
			} else {
				error("Failed to rise fleet of ``fleetSize`` from ``from``, by ``owner``");
			}
		}
		
		shared void commit() {
			assert(prepared);
			value fleet = detachedFleet;
			if (exists fleet) {
				to.addFleet(fleet);
			}
		}
		
		shared void rollback() {
			value fleet = detachedFleet;
			if(exists fleet) {
				from.addFleet(fleet);
			}
		}
		
		shared actual String string => "Move: { to: ``to.name``, from: ``from.name``, num: ``fleetSize``, by: ``owner.name`` }";
		
	}
	
	shared [Player*] players => playersList.sequence;
	shared [Planet*] planets =>  planetsList.sequence;
	shared [Planet*] getConnectedPlanets(Planet plant) {
		return (connectedPlanetsMap.get(plant) else LinkedList<Planet>()).sequence;
	}
	
	shared Planet? getPlanetByName(String name) {
		return planets.find((Planet p) => p.name == name);
	}
	
	shared void addPlayers(Player* players) {
		for (player in players) {
			assert(!player in this.players);
		}
		this.playersList.addAll(players);
	}
	
	shared void submitMove(Move move) {
		assert(move.from in planets);
		assert(move.to in planets);
		assert(move.player in players);
		moves.add(move);
	}
	
	shared void addPath(Path path) {
		void link(Planet from, Planet to) {
			value planets = connectedPlanetsMap.get(from) else LinkedList<Planet>();
			assert(!from in planets);
			assert(!to in planets);
			planets.add(to);
			connectedPlanetsMap.put(from, planets);
		}
		assert (path.planetA in planets && path.planetB in planets);
		link(path.planetA, path.planetB);
		link(path.planetB, path.planetA);
	}

	shared void addPlanets(Planet* planets) {
		for (planet in planets) {
			assert(planet.owner in players);
			assert(!planet in planetsList);
		}
		planetsList.addAll(planets);
	}
}

void error(String message) {
	print(message);
}


shared void main() {
	print(102);
//	value serverSocket = newServerSocket(SocketAddress("EPRURYAW0450", 1111));
	value serverSocket = newServerSocket(SocketAddress("EPRURYAW0450", 1111));
	//Player t = HumanPlayer("T", serverSocket);
	//Player p = HumanPlayer("Q", serverSocket);
	Player zerg = NormalPlayer("Zerg");
	Player terran = NormalPlayer("Terran");
	Player protoss = NormalPlayer("Protoss");
	//Player greenMen = NormalPlayer("Marsian");
	Player greenMen = AiPlayer("QQ");
	Player neutral = NeutralPlayer();

	value world = generateWorld({ zerg, terran, protoss, greenMen}, neutral);
	//value world = generateWorld({t, p, zerg, terran, protoss, greenMen}, neutral);
	//value world = generateWorld({t, p}, neutral);
	
	print("start");
	world.go();
}