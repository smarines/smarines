import ceylon.language.meta.model { Function }
import ceylon.collection { HashSet, HashMap }

void foo() {
/*

 
 
 */	
}

void doFancyStuff(World world, Player owner) {
	Planet root = nothing;
	

}


shared abstract class RequestType() of populate | populateNew | attack | attackAndDestroy | prepareAttack {
}

object populate extends RequestType() {}
object populateNew extends RequestType() {}
object attack extends RequestType() {}
object attackAndDestroy extends RequestType() {}
object prepareAttack extends RequestType() {}


shared class Request(shared FleetSize totalReqestSize, shared RequestType type) {
	value surplus = HashMap<Planet, FleetSize>();
	value network = HashMap<Planet, FleetSize>();
	variable FleetSize fulfilledSize = FleetSize(0);
	variable FleetSize currentSize = totalReqestSize;
	shared void fulfillFromSurplus(FleetSize number, Planet p) {
		surplus.put(p, number);
		try {	
			value cs = currentSize.substract(number);
			currentSize = (cs else nothing);
		} catch (Exception e) {
			print(e);
		}
	}
	
	shared void fulfillFromNetwork(FleetSize number, Planet p) {
		network.put(p, number);
		currentSize = (currentSize.substract(number) else nothing);
	}
	
	shared FleetSize size => currentSize;
	shared FleetSize fsize => totalReqestSize.substract(currentSize) else nothing;
	
}
shared class PullRequest(FleetSize size, shared Planet consumer) extends Request(size, populate) {}

shared class AttackRequest(shared FleetSize size, shared Planet from, shared Planet to) {
	
}

class AiPlanet(Planet p, World w) {
	shared Planet rawPlanet = p;
	shared World rawWorld = w;
	shared FleetSize maxBuildSize => p.getOwnerFleet().size.mul(p.buildRatio.float) else nothing;
	shared FleetSize currentBuildSize {
		value max = maxBuildSize;
		value current = p.getOwnerFleet().size;
		value capacity = p.capacity;
		if (current.add(max) <= capacity) {
			return max;
		} else if (current.add(max) > capacity) {
			return FleetSize(0);
		} else {
			return capacity.substract(current) else nothing;
		}
	}
	// если планета больше то всегда стоит удовлетворять реквест полностью
	// пушить излишки на самого маленького соседа?
	// разнве векторы не учитывают количеств уже улетевших кораблей
	
	shared FleetSize extraShips {
		value bestSize = p.capacity.mul(1.float / (p.buildRatio.float + 1.float)) else nothing;
		value ownerFleetSize = p.getOwnerFleet().size;
		if (bestSize >= ownerFleetSize) {
			return FleetSize(0);
		} else {
			return ownerFleetSize.substract(bestSize) else nothing;
		}
	}
	
	shared FleetSize maxDetachableSize => p.getOwnerFleet().size.substract(FleetSize(1)) else nothing;
	
	shared Boolean isEnemyFrontier {
		return !enemyPlanets.empty;
	}

	shared Boolean isFrontier {
		return !enemyPlanets.empty || !neutralPlanets.empty;
	}
	AiPlanet toAiPlanet(Planet p) => AiPlanet(p, w);
	shared {AiPlanet*} ownerPlanets {
		return w.getConnectedPlanets(p).filter((Planet elem) => elem.owner == p.owner).map(toAiPlanet);
	}
	
	shared {AiPlanet*} enemyPlanets {
		return w.getConnectedPlanets(p).filter((Planet elem) => elem.owner != p.owner && !elem.owner is NeutralPlayer()).map(toAiPlanet);
	}
	
	shared {AiPlanet*} neutralPlanets {
		return w.getConnectedPlanets(p).filter((Planet elem) => elem.owner is NeutralPlayer()).map(toAiPlanet);
	}
	
	shared {AiPlanet*} nonNeutralPlanets {
		return w.getConnectedPlanets(p).filter((Planet elem) => !elem.owner is NeutralPlayer()).map(toAiPlanet);
	}
	
	shared Integer numberOfEnemies {
		value allPlayers = enemyPlanets.map((AiPlanet element) => element.rawPlanet.owner);
		value players = HashSet<Player>();
		players.addAll(allPlayers);
		return players.size;
	}
	
	shared {Player*} connectedPlayers(Player perspective) {
		return nonNeutralConnectedPlayers.filter((Player elem) => elem != perspective);
	}
	
	shared {Player*} nonNeutralConnectedPlayers {
		return nonNeutralPlanets.map((AiPlanet p) => p.rawPlanet.owner);
	}
	
	shared actual Boolean equals(Object that) {
		if (is AiPlanet that) {
			return that.rawPlanet == this.rawPlanet;
		} else {
			return false;
		}
	}
	
	shared actual String string => rawPlanet.string;

}

class AiWorld(shared World world) {
	
	shared {Planet*} frontiers(Player owner) => playerPlanets(owner).filter((Planet elem) => AiPlanet(elem, world).isFrontier);
	shared {Planet*} firstRow(Player owner) => playerPlanets(owner).filter((Planet elem) => !AiPlanet(elem, world).isFrontier && world.getConnectedPlanets(elem).containsAny(frontiers(owner)));
	shared {Planet*} core(Player owner) => playerPlanets(owner).filter((Planet elem) => !frontiers(owner).contains(elem) && !firstRow(owner).contains(elem));
	shared {Planet*} playerPlanets(Player player) => world.planets.filter((Planet elem) => elem.owner == player);
	shared {Planet*} connectedplanetsFor(Planet planet) => world.getConnectedPlanets(planet);
	shared {Planet*} connectedEnemyPlanets(Player owner) {
		return frontiers(owner).fold(HashSet<Planet>(), (HashSet<Planet> partial, Planet elem) {
			partial.addAll(world.getConnectedPlanets(elem).filter((Planet elem) => elem.owner != owner)); 
			return partial;
		}).sequence;
	}
	
	shared {Player*} connectedPlayers(Player player) {
		return connectedEnemyPlanets(player).fold(HashSet<Player>(), (HashSet<Player> partial, Planet elem) {
			partial.add(elem.owner);
			return partial;
		}).sequence;	
	}
	
	shared Integer numberOfConnectedEnemies(Player player) {
		return connectedPlayers(player).size;
	}
}


FleetSize calculateFleetSize(World world, Player owner) {
	return calculateForOwnerPlanets(world, owner, FleetSize(0), (FleetSize acc, Planet p) => acc.add(p.getOwnerFleet().size));
}

FleetSize calculateMaxRegen(World world, Player owner) {
	return calculateForOwnerPlanets(world, owner, FleetSize(0), (FleetSize size, Planet planet) { 
		return size.add(planet.getOwnerFleet().size); 
	});
}

T calculateForOwnerPlanets<T>(World world, Player owner, T init, T func(T acc, Planet p)) {
	return world.planets.filter((Planet p) => p.owner == owner).fold(init, func);
}


class ApiPlanet(Planet planet) {
}