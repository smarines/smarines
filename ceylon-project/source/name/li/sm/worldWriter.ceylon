import java.io { StringWriter }
import javax.xml.stream { XMLOutputFactory, XMLStreamWriter }

shared void test() {

}

shared String worldToXml(World world) {
	value sw = StringWriter();
	XMLOutputFactory factory = XMLOutputFactory.newFactory();
	value writer = factory.createXMLStreamWriter(sw);
	
	writer.writeStartDocument();
	writer.writeStartElement("response");
	writePlanets(writer, world);
	writer.writeEmptyElement("errors");
	writer.writeEndElement();
	return sw.string;
}

void writePlanets(XMLStreamWriter writer, World world) {
	writer.writeStartElement("planets");
	for (planet in world.planets) {
		writePlanet(writer, planet, world.getConnectedPlanets(planet));
	}
	writer.writeEndElement();
}

void writePlanet(XMLStreamWriter writer, Planet planet, {Planet*} linkedPlanets) {
	writer.writeStartElement("planet");
	writer.writeAttribute("id", planet.name);
	writeOwner(writer, planet.owner);
	writePlanetType(writer, planet);
	writeDroids(writer, planet);
	writeLinkedPlanets(writer, linkedPlanets);
	writer.writeEndElement();
}

void writeOwner(XMLStreamWriter writer, Player player) {
	writer.writeStartElement("owner");
	writer.writeCharacters(player.name);
	writer.writeEndElement();
}

void writePlanetType(XMLStreamWriter writer, Planet planet) {
	writer.writeStartElement("type");
	//writer.writeCharacters(planet.buildRatio.size.string);
	value ratio = planet.buildRatio;
	if (ratio == 0.1) {
		writer.writeCharacters("TYPE_A");
	} else if (ratio == 0.15) {
		writer.writeCharacters("TYPE_B");
	} else if (ratio == 0.2) {
		writer.writeCharacters("TYPE_C");
	} else if (ratio == 0.3) {
		writer.writeCharacters("TYPE_D");
	} else {
		writer.writeCharacters("UNKNOWN");
	}
	writer.writeEndElement();
}

void writeDroids(XMLStreamWriter writer, Planet planet) {
	writer.writeStartElement("droids");
	writer.writeCharacters(planet.getOwnerFleet().size.string);
	writer.writeEndElement();
}

void writeLinkedPlanets(XMLStreamWriter writer, {Planet*} linkedPlanets) {
	writer.writeStartElement("neighbours");
	for (planet in linkedPlanets) {
		writer.writeStartElement("neighbour");
		writer.writeCharacters(planet.name);
		writer.writeEndElement();
	}
	writer.writeEndElement();
}

