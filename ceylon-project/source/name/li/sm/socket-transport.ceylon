import ceylon.io { Socket, SocketAddress, newServerSocket }
import ceylon.io.charset { utf8 }
import ceylon.io.buffer { newByteBuffer, newByteBufferWithData }
import org.xml.sax.helpers { DefaultHandler }
import java.lang { Thread }
shared void zzz() {
	print(getWorldXml());
	print("start");
	value serverSocket = newServerSocket(SocketAddress("localhost", 333));
	value socket = serverSocket.accept();
	value reader = Reader(socket);
	value xml = reader.nextToken();
	print(xml);
	print(parseRequest(xml.trimmed));
	socket.writeFully(utf8.encode(getWorldXml()));
	Thread.sleep(1000);
}

shared String getWorldXml() {
	Player me = NormalPlayer("me");
	Player someoneElse = NormalPlayer("notme");
	Player neutral = NeutralPlayer();
	
	value earth = Planet("Earth", FleetSize(1000), 0.2, me, FleetSize(50));
	value mars = Planet("Mars", FleetSize(500), 0.1, someoneElse, FleetSize(200));
	value moon = Planet("moon", FleetSize(200), 0.5, neutral);
	
	value world = World();
	world.addPlayers(me, someoneElse);
	world.addPlanets(earth, mars, moon);
	world.addPath(Path(earth, moon));
	world.addPath(Path(moon, mars));
	return worldToXml(world);
}


class MyParser() extends DefaultHandler() {
}



shared void test111() {
	print("asd".endsWith("d"));
}



class Reader(Socket socket) {
	variable value buffer = newByteBuffer(0);
	
	shared String nextToken() {
		String str = utf8.decode(buffer);
		buffer.flip();
		if (str.trimmed.endsWith(">")) {
			return str.trimmed;
		} else {
			Thread.sleep(100);
			read();
			return nextToken();
		}
	}
	
	void read() {
		socket.setNonBlocking();
		variable {Integer*} collectedBytes = {};
		variable value tempBuffer = newByteBuffer(0);
		variable value bytesRead = tempBuffer.size;
		variable value firstRun = true;
		while (tempBuffer.capacity == bytesRead && (bytesRead != 0 || firstRun)) {
			firstRun = false;
			tempBuffer = newByteBuffer(100);
			bytesRead = socket.read(tempBuffer);
			tempBuffer.flip();
			if (bytesRead > 0) {
				print(utf8.decode(tempBuffer));
				tempBuffer.flip();
				collectedBytes = collectedBytes.chain(tempBuffer.sequence);
			}
			tempBuffer.flip();
		}
		buffer = newByteBufferWithData(*collectedBytes);
	}
	
}