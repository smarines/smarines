import java.io { StringReader }
import javax.xml.parsers { DocumentBuilder, DocumentBuilderFactory }
import org.w3c.dom { Document, Node, Element }
import org.xml.sax { InputSource }

shared void testReqParser() {
	String val = 
			"""<?xml version="1.0" encoding="UTF-8"?>
			   <request>
			   <token>abcdefghijklmnopqrstuvwxy</token>
			   <actions>
			   <action>
			   <from>15</from>
			   <to>25</to>
			   <unitscount>1200</unitscount>
			   </action>
			   <action>
			   <from>15</from>
			   <to>23</to>
			   <unitscount>100</unitscount>
			   </action>
			   <action>
			   <from>15</from>
			   <to>21</to>
			   <unitscount>105</unitscount>
			   </action>
			   </actions>
			   </request>
	                """;
}

shared {MoveStub*} parseRequest(String str) {
	DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
	DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
	Document doc = dBuilder.parse(InputSource(StringReader(str)));
	value actionElems = doc.getElementsByTagName("action");
	variable value index = 0;
	variable [MoveStub*] moves = [];
	value size = actionElems.length;
	value token =  parseToken(doc);
	while(index != size) {
		value elem = actionElems.item(index);
		MoveStub action = parseAction(elem, token);
		moves = moves.withLeading(action);
		index++;
	}
	return moves.sequence;
}

String parseToken(Document doc) {
	value e = doc.getElementsByTagName("token").item(0);
	String token = e.textContent;
	return token;
}
MoveStub parseAction(Node elem, String token) {
	value children = elem.childNodes;
	variable value index = 0;
	variable String to = "stub";
	variable String from = "stub";
	variable String num = "stub";
	while(index != children.length) {
		print("index " + index.string);
		value subElem = children.item(index);
		if (subElem is Element) {
			if (subElem.nodeName == "to") {
				to = subElem.textContent;
			} else if (subElem.nodeName == "from") {
				from = subElem.textContent;
			} else if (subElem.nodeName == "unitscount") {
				num = subElem.textContent;
			}
		}
		index++;
	}
	return MoveStub(to, from, num, token); 
}