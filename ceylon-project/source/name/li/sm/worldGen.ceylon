import ceylon.collection { LinkedList, HashSet }
import ceylon.file { parsePath, File }

shared World generateWorld({Player*} players, Player neutralPlayer = NeutralPlayer()) {

	value world = World();
	value allPlayers = players.chain([ neutralPlayer ]);
	world.addPlayers(*allPlayers);
	value homeWorlds = LinkedList<Planet>();
	value frontiers = LinkedList<Planet>();
	value firstRights = LinkedList<Planet>();
	value secondRights = LinkedList<Planet>();
	value middle = getLeveledPlanet { mega; owner = neutralPlayer; name = "Mega"; };

	for (player in players) {
		homeWorlds.add(getLeveledPlanet(small, player, "``player.name`` Homeworld", FleetSize(100)));
		frontiers.add(getLeveledPlanet { medium; owner = neutralPlayer; name = "``player.name`` Frontier"; });
		firstRights.add(getLeveledPlanet(medium, neutralPlayer, "``player.name`` firstRight"));
		secondRights.add(getLeveledPlanet(large, neutralPlayer, "``player.name`` secondRight"));
	}
	
	void cycle(LinkedList<Planet> list) {
		list.add(list.first else nothing);
	}
	cycle(homeWorlds);
	cycle(frontiers);
	cycle(firstRights);
	cycle(secondRights);
	
	value allPlanets = homeWorlds.chain(frontiers).chain(firstRights).chain(secondRights).chain([ middle ]);
	world.addPlanets(*HashSet(allPlanets).sequence);
	
	for (index -> thing in entries(homeWorlds)) {
		value idx = index + 1;
		if(idx >= homeWorlds.size) {
			break;
		}
		world.addPath(constructPath(idx, idx, homeWorlds, frontiers));
		world.addPath(constructPath(idx, idx, frontiers, firstRights));
		world.addPath(constructPath(idx, idx - 1, frontiers, firstRights));
		world.addPath(constructPath(idx, idx, frontiers, secondRights));
		world.addPath(constructPath(idx, idx - 1, frontiers, secondRights));
		world.addPath(constructPath(idx, idx, firstRights, secondRights));
		world.addPath(Path(secondRights.get(idx) else nothing, middle));
	}
	
	return world;
}

Path constructPath(Integer index1, Integer index2, LinkedList<Planet> list1, LinkedList<Planet> list2) {
	try {
		return Path(list1.get(index1) else nothing, list2.get(index2) else nothing);
	} catch (Exception e) {
		print(index1.string + " " + index2.string + " " + list1.string + " " + list2.string);
		return nothing;
	}
}

Planet getLeveledPlanet(PlanetLevel level, Player owner, String name, FleetSize initialFleetSize = FleetSize(0)) {
	return Planet(resolvePlanetName(owner) else name, level.capacity, level.buildRatio, owner, initialFleetSize);
}

shared String? resolvePlanetName(Player p) {
	value name = p.name.lowercased;
	if (name == "protoss") {
		return "Aiur";
	} else if (name == "zerg") {
		return "Zerus";
	} else if (name == "terran") {
		return "Terra";
	} else if (name == "marsian") {
		return "Mars";
	} else {
		return null;
	}
}

shared abstract class PlanetLevel(shared FleetSize capacity, shared Float buildRatio) 
		of small | medium | large | mega {
	
}
		
object small extends PlanetLevel(FleetSize(100), 0.1) {}
object medium extends PlanetLevel(FleetSize(200), 0.15) {}
object large extends PlanetLevel(FleetSize(500), 0.2) {}
object mega extends PlanetLevel(FleetSize(1000), 0.3) {}
